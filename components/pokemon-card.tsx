import React from "react";
import { PokemonDetails } from "../types";
import Link from "next/link";
import Image from "next/image";

const PokemonCard = (pokemon: PokemonDetails) => {
  return (
    <Link key={pokemon.id} href={`/pokemon/${pokemon.species.name}`}>
      <a key={pokemon.id} className="group">
        <div className="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
          <Image
            src={pokemon.image}
            alt={pokemon.species.name}
            width={215}
            height={215}
            className="w-full h-full object-center object-cover group-hover:opacity-75"
          />
        </div>
        <div className="flex justify-end mt-2">
          <h3 className="text-base font-bold text-gray-700">
            {pokemon.species.name}
          </h3>
        </div>
        <div className="grid grid-cols:1 mt-2">
          {pokemon.abilities.map((item, index) => (
            <h3
              key={index}
              className="justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 mt-2"
            >
              {item.ability.name}
            </h3>
          ))}
        </div>
      </a>
    </Link>
  );
};

export default PokemonCard;
