import React from "react";
import Image from "next/image";
import { IPokemonDetails } from "../types";

const PokemonDetail: React.FC<IPokemonDetails> = (
  pokemonDetails: IPokemonDetails
) => {
  const {
    id,
    abilities,
    stats,
    weight,
    height,
    image: pokemonImage,
    pokemonName,
    heldItems,
    forms,
  } = pokemonDetails;
  console.log("details", pokemonDetails);
  const pokemonTable = {
    id,
    abilities,
    stats,
    weight,
    height,
    heldItems,
    forms,
  };
  return (
    <div className="flex justify-center mt-10">
      {/* <div>{JSON.stringify(pokemonDetails, null, 2)}</div> */}
      <div className="bg-white rounded-lg shadow-2x1 w-3/4">
        <header className="bg-gray-100 rounded-t-lg py-3 px-8 text-xl font-extrabold">
          <h1>
            {pokemonName}-#{id}
          </h1>
        </header>
        <div className="flex justify-center">
          <Image
            src={pokemonImage}
            alt={pokemonName}
            width={215}
            height={215}
            className="w-full h-full object-center object-cover group-hover:opacity-75"
          />
        </div>
        <div className="bg-white shadow overflow-hidden sm:rounded-lg">
          <div className="px-4 py-5 sm:px-6">
            <h3 className="text-lg leading-6 font-medium text-gray-900">
              Details
            </h3>
            <p className="mt-1 max-w-2xl text-sm text-gray-500">
              Details about {pokemonName}
            </p>
          </div>
          <div className="border-t border-gray-200">
            {Object.entries(pokemonTable).map(([key, value]) => (
              <dl key={key}>
                <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                  <dt className="text-sm font-medium text-gray-500">{key}</dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {typeof value !== "object" ? value : JSON.stringify(value)}
                  </dd>
                </div>
              </dl>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PokemonDetail;
