import { getAllPokemon, getAllPokemonDetails } from "../services/pokemon";
import { useState } from "react";
import { InferGetServerSidePropsType } from "next";
import PokemonCard from "../components/pokemon-card";

export const getServerSideProps = async () => {
  const { next, results } = await getAllPokemon(0, 12);
  const pokemonDetails = await getAllPokemonDetails(results);
  return {
    props: { next, results, pokemonDetails },
  };
};

const Home = ({
  next,
  pokemonDetails,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [data, setData] = useState(pokemonDetails);
  const [nextUrl, setNextUrl] = useState(next);

  const handleLoadMore = async () => {
    const res = await fetch(nextUrl);
    const { next, results } = await res.json();
    setNextUrl(next);
    const pokemonResults = await getAllPokemonDetails(results);
    setData([...data, ...pokemonResults]);
  };

  return (
    <div>
      <div className="bg-white">
        <div className="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
          <h2 className="text-2xl mb-8">Pokedex</h2>
          <div className="grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
            {data.map((pokemon) => (
              <>
                <PokemonCard
                  id={pokemon.id}
                  species={pokemon.species}
                  image={pokemon.image}
                  abilities={pokemon.abilities}
                />
              </>
            ))}
          </div>
          <div className="grid justify-items-center mt-8 ">
            <button
              type="button"
              className="w-80 inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
              onClick={handleLoadMore}
            >
              Load more
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
