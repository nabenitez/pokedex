import React from "react";
import { InferGetServerSidePropsType } from "next";
import { getPokemonDetail } from "../../services/pokemon";
import PokemonDetail from "../../components/pokemon-detail";

export const getServerSideProps = async (context: { query: { name: any } }) => {
  const { name } = context.query;
  if (name && typeof name === "string") {
    const pokemonDetails = await getPokemonDetail(name);
    return {
      props: { pokemonDetails },
    };
  }
};

const PokemonDetailPage = ({
  pokemonDetails,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const {
    id,
    abilities,
    stats,
    weight,
    height,
    image,
    pokemonName,
    heldItems,
    forms,
  } = pokemonDetails;
  return (
    <PokemonDetail
      id={id}
      abilities={abilities}
      stats={stats}
      weight={weight}
      height={height}
      image={image}
      pokemonName={pokemonName}
      heldItems={heldItems}
      forms={forms}
    />
  );
};

export default PokemonDetailPage;
