import { IPokemonData, PokemonDetails, IPokemonDetails } from "../types";
const baseUrl = "https://pokeapi.co/api/v2/";

export const getAllPokemon = async (offset: Number, limit: Number) => {
  const res = await fetch(`${baseUrl}pokemon?offset=${offset}&limit=${limit}`);
  return res.json();
};

export const getAllPokemonDetails = async (
  pokemonData: IPokemonData[]
): Promise<PokemonDetails[]> => {
  const res = pokemonData.map(async (pokemon) => {
    const res = await fetch(pokemon.url);
    const { abilities, id, species } = await res.json();
    const image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
    return { abilities, id, species, image };
  });
  return await Promise.all(res);
};

export const getPokemonDetail = async (
  name: string
): Promise<IPokemonDetails> => {
  const res = await fetch(`${baseUrl}pokemon/${name}`);
  const {
    id,
    abilities,
    stats,
    held_items: heldItems,
    name: pokemonName,
    forms,
    height,
    weight,
  } = await res.json();
  const image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
  return {
    id,
    abilities,
    stats,
    heldItems,
    pokemonName,
    image,
    forms,
    height,
    weight,
  };
};
