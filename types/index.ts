export interface IPokemonData {
  name: string;
  url: string;
}

export interface IPokemonDetails {
  id: number;
  abilities: PokemonAbility[];
  stats: { base_stat: number; effort: number; stat: Stat };
  weight: number;
  height: number;
  image: string;
  pokemonName: string;
  heldItems: any[];
  forms: any[];
}

export type Stat = {
  name: string;
  url: string;
};

export type PokemonAbility = {
  ability: { name: string; url: string };
};

export type PokemonDetails = {
  abilities: PokemonAbility[];
  id: number;
  species: { name: string; url: string };
  image: string;
};
